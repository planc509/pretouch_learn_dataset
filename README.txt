In the 'Images' folder, each rgb image is labeled with an index, and the corresponding depth image has the same index followed by a 'd'. The rectangles can be found under the 'Annotations' folder, in the file with the same index. Each line of that file represents a single rectange with the following format:
	[x1,y1,x2,y2,theta]
where the first four values are in pixels, and theta is in radians betweeb -pi/2 and pi/2. The final rectangle is obtained by taking the rectangle defined by the diagonal from (x1,y1) to (x2,y2) and rotating it around its center by theta radians.
